package br.com.swingutil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public abstract class CustomTableModel<T> extends AbstractTableModel{
		
	private static final long serialVersionUID = 1L;
	
	protected String[] nomes;
	protected List<T> valores;

	public CustomTableModel(String[] nomes, List<T> valores) {
		this.nomes = nomes;
		this.valores = valores == null ? valores : new ArrayList<T>();
	}

	@Override
	public int getRowCount() {
		return this.valores.size();
	}

	@Override
	public int getColumnCount() {
		return nomes.length;
	}
	
	public boolean removeRows(int[] rows){
		boolean isRemoved = false;
		
		for (int i = 0; i < rows.length; i++) {
			
			if(isIndiceValido(i)){
				this.valores.remove(i);
				isRemoved = true;
			}
		}
		
		if(isRemoved){
			fireTableDataChanged();
		}
		
		return isRemoved;
	}
	
	public void addListRows(Collection<T> list){
		this.valores.addAll(list);
		fireTableDataChanged();
	}
	
	private boolean isIndiceValido(int i){
		return this.valores.size() > i && i > -1;
	}
	
	public T getEntity(int i){
		T t = null;
		
		if(this.isIndiceValido(i)){
			t = this.valores.get(i);
		}
		
		return t;
	}
	
	public int getSizeEntities(){
		return this.valores.size();
	}
	
	public void updateRow(T t){
		int indice = this.valores.lastIndexOf(t);
		if(indice > -1){
			this.valores.set(indice, t);
			fireTableDataChanged();
		}
	}
	
	public void clear(){
		this.valores.clear();
		fireTableDataChanged();
	}
}
