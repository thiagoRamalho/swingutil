package br.com.swingutil;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExitAction implements ActionListener {

	private Component component;
	
	public ExitAction(Component component) {
		super();
		this.component = component;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
	}

}
